from django.shortcuts import render,redirect
from . import forms, models
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt

def index(request):
    if request.method == "POST":
        if 'nama_kegiatan' in request.POST:
            formKegiatan = forms.KegiatanForm(request.POST)
            if formKegiatan.is_valid():
                context = models.Kegiatanku()
                context.nama_kegiatan = formKegiatan.cleaned_data['nama_kegiatan']
                context.save()
        elif 'nama_peserta' in request.POST and 'id_kegiatan' in request.POST:
            formPeserta = forms.PesertaForm(request.POST)
            if formPeserta.is_valid():
                models.Kegiatanku.objects.get(id=request.POST['id_kegiatan'])
                context = models.Pesertaku()
                context.nama_peserta = formPeserta.cleaned_data['nama_peserta']
                context.kegiatan = models.Kegiatanku.objects.get(id=request.POST['id_kegiatan'])
                context.save()
    
    lst_kegiatan = models.Kegiatanku.objects.all()
    data_lengkap = []
    for kegiatan in lst_kegiatan:
        peserta = models.Pesertaku.objects.filter(kegiatan = kegiatan)
        lst_peserta = []
        for i in peserta:
            lst_peserta.append(i)
        data_lengkap.append((kegiatan, lst_peserta))
    
    context= {
        'formKegiatan' : forms.KegiatanForm,
        'formPeserta' : forms.PesertaForm,
        'absen_kegiatan'  : data_lengkap
    }

    return render(request, 'main/home.html', context)
