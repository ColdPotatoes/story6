from django.test import TestCase , Client
from django.urls import resolve
from . import models
from .views import index
from .models import Kegiatanku
from .models import Pesertaku
from .models import *
from .forms import KegiatanForm , PesertaForm
from http import HTTPStatus
from django.apps import apps
from .apps import MainConfig

class UnitTestStory6(TestCase):
    
    def test_status(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_daftar_kegiatan(self):
        Kegiatanku.objects.create(nama_kegiatan = "koding SDA")
        Pesertaku.objects.create(nama_peserta = "sisDea")
        counter = Kegiatanku.objects.all().count()
        counter += Pesertaku.objects.all().count()
        self.assertEqual(counter, 2)

    def test_model_peserta(self):
        Pesertaku.objects.create(nama_peserta = "krox")
        peserta  = Pesertaku.objects.get(nama_peserta = "krox")
        self.assertEqual(str(peserta), "krox")

    def test_model_kegiatan(self):
        Kegiatanku.objects.create(nama_kegiatan = "SDA")
        kegiatan  = Kegiatanku.objects.get(nama_kegiatan = "SDA")
        self.assertEqual(str(kegiatan), "SDA")

    def test_form_benerga(self):
        data = {'nama_kegiatan':"mainApex"}
        kegiatan_form = KegiatanForm(data=data)
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"mainApex")

    def test_form_post(self):
        test_stringku = 'ian'
        response_post = Client().post('', {'nama_kegiatan':"ian"})
        self.assertEqual(response_post.status_code,200)
        kegiatan_form = KegiatanForm(data={'nama_kegiatan':test_stringku})
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"ian")

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_apps(self):
        self.assertEqual(MainConfig.name, 'main')
        self.assertEqual(apps.get_app_config('main').name, 'main')

    

  

