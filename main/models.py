from django.db import models

class Kegiatanku(models.Model):
    nama_kegiatan = models.CharField(max_length = 50, default = "")

    def __str__(self):
        return self.nama_kegiatan

class Pesertaku(models.Model):
    kegiatan = models.ForeignKey(Kegiatanku, on_delete= models.CASCADE,null = True, blank=True)
    nama_peserta = models.CharField(max_length = 50)

    def __str__(self):
        return self.nama_peserta
